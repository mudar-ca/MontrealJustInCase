/*
    Montréal Just in Case
    Copyright (C) 2011  Mudar Noufal <mn@mudar.ca>

    Geographic locations of public safety services. A Montréal Open Data
    project.

    This file is part of Montréal Just in Case.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.mtlaucasou.service;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;
import androidx.core.app.JobIntentService;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import ca.mudar.mtlaucasou.Const;
import ca.mudar.mtlaucasou.R;
import ca.mudar.mtlaucasou.api.ApiClient;
import ca.mudar.mtlaucasou.data.AppDatabase;
import ca.mudar.mtlaucasou.data.RoomQueries;
import ca.mudar.mtlaucasou.data.UserPrefs;
import ca.mudar.mtlaucasou.model.LayerType;
import ca.mudar.mtlaucasou.model.MapType;
import ca.mudar.mtlaucasou.model.geojson.FeatureCollection;
import ca.mudar.mtlaucasou.model.jsonapi.Attributes;
import ca.mudar.mtlaucasou.model.jsonapi.DataItem;
import ca.mudar.mtlaucasou.model.jsonapi.HelloApi;
import ca.mudar.mtlaucasou.util.ApiDataUtils;
import ca.mudar.mtlaucasou.util.LogUtils;
import retrofit2.Response;

import static ca.mudar.mtlaucasou.util.LogUtils.LOGV;
import static ca.mudar.mtlaucasou.util.LogUtils.makeLogTag;

public class SyncService extends JobIntentService {
    private static final String TAG = makeLogTag("SyncService");

    private AppDatabase mDatabase;

    public static void enqueueWork(Context context) {
        final Intent intent = new Intent(context, SyncService.class);
        enqueueWork(context, SyncService.class, Const.RequestCodes.SYNC_JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        mDatabase = AppDatabase.getAppDatabase(this);
        final long startTime = System.currentTimeMillis();

        final UserPrefs userPrefs = UserPrefs.getInstance(this);
        if (!userPrefs.hasLoadedData()) {
            loadInitialLocalData();
        } else {
            // TODO handle updates frequency and redundancy
            downloadRemoteUpdatesIfAvailable(userPrefs);
        }

        LOGV(TAG, String.format("Data sync duration: %dms", System.currentTimeMillis() - startTime));
    }

    private void loadInitialLocalData() {
        mDatabase.runInTransaction(() -> {
            importLocalData(R.raw.fire_halls, MapType.FIRE_HALLS, LayerType.FIRE_HALLS);
            importLocalData(R.raw.spvm_stations, MapType.SPVM_STATIONS, LayerType.SPVM_STATIONS);
            importLocalData(R.raw.spvm_stations_polygons, MapType.SPVM_STATIONS, LayerType.SPVM_AREAS);
            importLocalData(R.raw.water_supplies, MapType.HEAT_WAVE, LayerType._HEAT_WAVE_MIXED);
            importLocalData(R.raw.air_conditioning, MapType.HEAT_WAVE, LayerType.AIR_CONDITIONING);
            importLocalData(R.raw.emergency_hostels, MapType.EMERGENCY_HOSTELS, LayerType.EMERGENCY_HOSTELS);
            importLocalData(R.raw.hospitals, MapType.HEALTH, LayerType.HOSPITALS);
            importLocalData(R.raw.clsc, MapType.HEALTH, LayerType.CLSC);
        });
    }

    private void downloadRemoteUpdatesIfAvailable(UserPrefs prefs) {
        try {
            Response<HelloApi> helloResponse = ApiClient.hello(ApiClient.getService());
            if (helloResponse != null && helloResponse.body() != null) {
                final HelloApi api = helloResponse.body();
                for (DataItem item : api.getData()) {
                    final String key = ApiDataUtils.getSharedPrefsKey(item.getId());
                    final Date updatedAt = item.getAttributes().getUpdated();

                    if (prefs.isApiDataNewer(key, updatedAt)) {
                        final boolean result = importRemoteData(item);
                        if (result) {
                            prefs.setDataUpdatedAt(key, updatedAt);
                        }
                    }
                }
            }

        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    private void importLocalData(@RawRes int resource, @MapType String mapType, @LayerType String layerType) {
        final InputStream inputStream = getResources().openRawResource(resource);
        final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        final Gson gson = ApiClient.getGsonBuilder().create();
        final FeatureCollection collection = gson.fromJson(inputStreamReader,
                FeatureCollection.class);

        RoomQueries.cacheMapData(mDatabase,
                collection.getFeatures(),
                mapType,
                layerType);
    }

    /**
     * Request the GeoJSON data from the API
     */
    private boolean importRemoteData(DataItem dataItem) {
        final Response<FeatureCollection> response = ApiClient
                .getPlacemarks(ApiClient.getService(), dataItem.getLinks().getSelf());

        if (response != null) {
            FeatureCollection collection = response.body();
            if (collection != null && collection.getFeatures() != null) {
                final Attributes attributes = dataItem.getAttributes();

                if (attributes != null) {
                    RoomQueries.clearMapData(mDatabase, attributes.getLayerType());
                    RoomQueries.cacheMapDataWithTransaction(mDatabase,
                            collection.getFeatures(),
                            attributes.getMapType(),
                            attributes.getLayerType());
                }
            }

            return true;
        }

        return false;
    }
}
