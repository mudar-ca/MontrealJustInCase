/*
    Montréal Just in Case
    Copyright (C) 2011  Mudar Noufal <mn@mudar.ca>

    Geographic locations of public safety services. A Montréal Open Data
    project.

    This file is part of Montréal Just in Case.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.mtlaucasou.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import ca.mudar.mtlaucasou.Const;
import ca.mudar.mtlaucasou.R;
import ca.mudar.mtlaucasou.ui.fragment.SettingsFragment;
import ca.mudar.mtlaucasou.util.MetricsUtils;

public class SettingsActivity extends BaseActivity implements
        SettingsFragment.LanguageChangeListener {
    public static Intent newIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle(R.string.title_activity_settings);

        if (savedInstanceState == null) {
            final Fragment fragment = SettingsFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, fragment, Const.FragmentTags.SETTINGS)
                    .commit();
        }

        MetricsUtils.logSettingsView();
    }

    /**
     * Update the interface language, independently from the phone's UI
     * language.
     */
    @Override
    public void onLanguageChanged(String lg) {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        startActivity(newIntent(this));
    }
}
