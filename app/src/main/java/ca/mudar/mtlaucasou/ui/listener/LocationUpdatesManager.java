/*
    Montréal Just in Case
    Copyright (C) 2011  Mudar Noufal <mn@mudar.ca>

    Geographic locations of public safety services. A Montréal Open Data
    project.

    This file is part of Montréal Just in Case.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.mtlaucasou.ui.listener;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener;
import com.google.android.gms.tasks.OnCompleteListener;

import ca.mudar.mtlaucasou.ui.adapter.PlacemarkInfoWindowAdapter;
import ca.mudar.mtlaucasou.util.MapUtils;
import ca.mudar.mtlaucasou.util.PermissionUtils;

import static ca.mudar.mtlaucasou.util.LogUtils.makeLogTag;

public class LocationUpdatesManager implements DefaultLifecycleObserver,
        PlacemarkInfoWindowAdapter.InfoWindowLocationCallbacks {

    private static final String TAG = makeLogTag("LocationUpdatesManager");
    private static final long LOCATION_UPDATES_INTERVAL = DateUtils.SECOND_IN_MILLIS * 10;
    private static final long LOCATION_UPDATES_FASTEST_INTERVAL = DateUtils.SECOND_IN_MILLIS * 5;

    private final ContextWrapper context;
    private LocationUpdatesCallbacks mListener;

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mUserLocation;
    private boolean mHasCameraMoved;
    private final OnCameraMoveStartedListener cameraMoveListener = new OnCameraMoveStartedListener() {
        @Override
        public void onCameraMoveStarted(int reason) {
            if (reason == REASON_GESTURE) {
                mHasCameraMoved = true;
            }
        }
    };
    private OnCompleteListener<LocationSettingsResponse> locationSettingsListener = task -> {
        try {
            final LocationSettingsResponse response = task.getResult(ApiException.class);
            onLocationSettingsResult(Activity.RESULT_OK, null);
        } catch (ApiException exception) {
            switch (exception.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied. But could be fixed by showing the
                    // user a dialog.
                    try {
                        // Cast to a resolvable exception.
                        ResolvableApiException resolvable = (ResolvableApiException) exception;
                        mListener.requestLocationSettingsChange(resolvable);
                    } catch (IntentSender.SendIntentException | ClassCastException e) {
                        // Ignore the error
                        e.printStackTrace();
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    // Location settings are not satisfied. However, we have no way to fix the
                    // settings so we won't show the dialog.
                    break;
            }
        }
    };
    private final LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null || locationResult.getLocations().isEmpty()) {
                return;
            }

            if (mFusedLocationClient != null) {
                mFusedLocationClient.removeLocationUpdates(this);
            }

            moveMapToLocation(locationResult.getLastLocation());
        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            super.onLocationAvailability(locationAvailability);
            if (!locationAvailability.isLocationAvailable()) {
                checkLocationSettings();
            }
        }
    };

    public LocationUpdatesManager(LocationUpdatesCallbacks listener) {
        this.mListener = listener;
        this.context = new ContextWrapper(listener.getActivity().getApplicationContext());

        listener.getActivity().getLifecycle().addObserver(this);
    }

    private static LocationRequest getDefaultLocationRequest() {
        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(LOCATION_UPDATES_INTERVAL);
        locationRequest.setFastestInterval(LOCATION_UPDATES_FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        return locationRequest;
    }

    @Override
    public void onStart(@NonNull LifecycleOwner owner) {
        setupInitialLocation();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        mFusedLocationClient.removeLocationUpdates(locationCallback);
        mFusedLocationClient = null;
    }

    @Override
    public void onDestroy(@NonNull LifecycleOwner owner) {
        mMap = null;
        mListener = null;
    }

    @Override
    @Nullable
    public Location getUserLocation() {
        return mUserLocation;
    }

    public void setGoogleMap(GoogleMap map) {
        mMap = map;

        mMap.setOnCameraMoveStartedListener(cameraMoveListener);
        moveMapToMyLocation();
    }

    public void onLocationSettingsResult(int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && PermissionUtils.checkLocationPermission(context)) {
            updateUserLocation();
        }
    }

    public void onLocationPermissionGranted() {
        setupInitialLocation();
    }

    private void setupInitialLocation() {
        final Activity activity = mListener.getActivity();
        if (activity == null) {
            return;
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        mFusedLocationClient.requestLocationUpdates(
                getDefaultLocationRequest(),
                locationCallback,
                activity.getMainLooper()
        );
    }

    private void updateUserLocation() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
                final boolean firstLocation = (mUserLocation == null);
                mUserLocation = location;
                if (firstLocation) {
                    moveMapToMyLocation();
                }
            });
        }
    }

    private void moveMapToLocation(Location location) {
        mUserLocation = location;
        if (location == null) {
            checkLocationSettings();
        } else {
            moveMapToMyLocation();
        }
    }

    private void checkLocationSettings() {
        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(getDefaultLocationRequest());

        LocationServices.getSettingsClient(context)
                .checkLocationSettings(builder.build())
                .addOnCompleteListener(locationSettingsListener);
    }

    private void moveMapToMyLocation() {
        if (!mHasCameraMoved && mUserLocation != null && mMap != null) {
            MapUtils.moveCameraToMyLocation(mMap, mUserLocation);
        }
    }

    public interface LocationUpdatesCallbacks {

        AppCompatActivity getActivity();

        void requestLocationSettingsChange(ResolvableApiException resolvableException) throws IntentSender.SendIntentException;

        void onLocationSettingsActivityResult(int resultCode, Intent data);
    }
}
